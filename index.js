require('dotenv').config()
const express = require('express');
const app = express();
const cors = require('cors');

const request = require('request');

// Configuration
const PORT = process.env.PORT;
const HOST = process.env.HOST;

app.use(express.json());
app.use(cors());
app.options('*', cors()) // include before other routes

function proxyCall(req, res) {

    request(process.env.API_SERVICE_URL + req.path, { 
        method: 'POST',
        json: true,
        headers: {
            "Authorization": "Bearer " + process.env.TOKEN
        },
        body: req.body
    }, (err, apiRes, apiBody) => {
        if (err) {  
            console.log(err); 
        }
        res.send(apiBody);
        res.end();
    });
}

app.post('/trigger/query', (req, res, next) => {
    proxyCall(req, res);
});

app.post('/participant/', (req, res, next) => {
    proxyCall(req, res);
});

 // Start the Proxy
app.listen(PORT, HOST, () => {
    console.log(`Starting Proxy at ${HOST}:${PORT}`);
 });
