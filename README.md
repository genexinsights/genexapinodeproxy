# Genex API Node Proxy #

This project is written in NodeJS Version v14.15.4.

The Genex API Node Proxy was designed for aiding testing with the genex-js-survey-modal. It simulates a client proxy on two routes, /trigger/query and /participant/.

---
## How to run this project


1. Create the .env file *(See How to create the .env file)*
2. run: npm install
3. run: npm start
4. The proxy will now run on localhost, port 3000

---
## How to create the .env file

All project settings are contained in the .env file.

1. Make a copy of the .env.example in the project route and name it '.env'
2. The ENV file should contain the following environment variables:
```dosini
PORT=3000
HOST=localhost
API_SERVICE_URL={genex_api_url}
TOKEN={your token}
```
3. Replace *{genex_api_url}* with the full Genex API path
4. Replace *{your token}* with the token provided to you by Genex

---
